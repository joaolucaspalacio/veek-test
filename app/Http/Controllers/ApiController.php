<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UserStoreRequest;
use App\User;

class ApiController extends Controller
{
    public function index()
    {
        return response()->json(User::all());
    }
 
    public function show($id)
    {
        return response()->json(User::find($id));
    }

    public function store(UserStoreRequest $request)
    {        
        return response()->json(User::create($request->all()));
    }

    public function update(UserStoreRequest $request, $id)
    {
        $user = User::find($id);
        if (isset($user) && $user->update($request->all())){
            return response()->json(['success'=>true]);
        }
        return response()->json(['errors'=>'Não foi possivel atualizar esse registro. Codigo inválido']);
    }

    public function destroy(Request $request, $id)
    {
        $user = User::find($id);
        if (isset($user) && $user->delete()){
            return response()->json(['success'=>true],200);
        }

        return response()->json(['errors'=>'Não foi possivel excluir esse registro. Codigo inválido'],400);
    }
}
