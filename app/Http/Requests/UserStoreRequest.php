<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'email' => 'required|email|unique:users',
                    'name' => 'required|string|max:150'
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [                    
                    'email'=>'required|email|unique:users,id,:id',
                    'name' => 'required|string|max:150'
                ];
            }
            default:break;        
        }
    }
    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'email.required' => 'Campo email obrigátorio!',
            'email.email' => 'Campo email deve conter um email valido!',
            'email.unique' => 'Esse email já está cadastrado!',
            'name.required' => 'Campo nome obrigátorio!',
        ];
    }

}
