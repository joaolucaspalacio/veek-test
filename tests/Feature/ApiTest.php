<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Faker\Factory;
use App\User;

class ApiTest extends TestCase
{
    use RefreshDatabase;
    
    public function testListAllUsers()
    {
        $response = $this->json('GET', 'api/users');
        $response->assertOk();
    }

    public function testCreateUser()
    {
        $faker = Factory::create();        
        $response = $this->json('POST', 'api/users',['name'=>$faker->name,'email'=>$faker->email]);
        $response->assertSuccessful();
    }

    public function testUpdate()
    {
        $user = factory(\App\User::class)->create();                
        $response = $this->json('PUT', 'api/users/'.$user->id,['email'=>'teste@test.com','name'=>'Veek Test']);
        $response->assertOk();
    }

    public function testUpdateUserWithoutEmailError()
    {
        $user = factory(\App\User::class)->create();                
        $response = $this->json('PUT', 'api/users/'.$user->id,['name'=>'Veek Test']);
        $response->assertStatus(422);
    }

    public function testDelete()
    {
        $user = factory(\App\User::class)->create();        
        $response = $this->json('DELETE', 'api/users/'.$user->id);
        $response->assertOk();
    }

}

